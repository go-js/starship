# README #

Starship game with HTML, CSS, and Vanilla JS

![screenshot](images/screenshot.jpg?raw=true "screenshot")

### Game description ###

Starship is an 'old Galaxy like' game, the aim of which is to shoot enemy spaceships.

### How to start ###

Download repository, run index.html on your local web server.